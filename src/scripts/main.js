/* Use a component to display the person details */
Vue.component('show-person', {
    props: [
        'currentPerson'
    ],
    template: 
    '<div v-if="currentPerson"><img :src="photo()" class="mt-2 photo float-left">'
    + '<h3 class="hidden md:block font-bold text-2xl">{{ currentPerson.firstName }} {{ currentPerson.lastName }}</h3>'
    + '<p class="markdown-body leading-5">{{ currentPerson.bio }}</p></div>',
    methods: {
        photo() {
            return this.currentPerson.picture;
        },
        fixData(string) {
            /* Remove code tags */
            return string.replace(/<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, "");
        }
    },
    mounted() {
        if (this.currentPerson) {
            this.currentPerson.bio = this.fixData(this.currentPerson.bio);
        }
    }
});

/* Load Vue page */
var app = new Vue({
    el: '#app',
    data: {
        peopleList: data,
        initPerson: 5,
        currentPerson: null,
    },
    methods: {
        isSelected(id){
            return this.currentPerson && this.currentPerson.id == id ? true : false;
        },
        selectPerson(id) {
            this.currentPerson = this.peopleList.find(item => item.id == id);
        },
        /* Sort array by key */
        sortList(list, key) {
            return list.sort(function (a, b) {
                if (a[key] < b[key])
                    return -1;
                if (a[key] > b[key])
                    return 1;
                return 0;
            });
        }
    },
    mounted() {
        this.peopleList = this.sortList(this.peopleList, 'firstName');
        this.selectPerson(this.initPerson);
    },

})